from flask import Flask, jsonify, request
from flask_mysqldb import MySQL
from dotenv import load_dotenv
load_dotenv()
# import de la bibiothèque cors
from flask_cors import CORS

app = Flask(__name__)
# ajout d'une configuration
CORS(app, resources={r"/*": {"origins": "http://localhost:8080"}})

app.config.from_object('config')
# Configuration de la connexion à la base de données
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'garadm7841' 
app.config['MYSQL_PASSWORD'] = 'SP7c3$@uwL84jmSEoP3' 
app.config['MYSQL_DB'] = 'flask_garage' 

# Crée un objet MySQL pour interagir avec la base de données
mysql = MySQL(app)

# Les futures routes seront ajoutées ici
@app.route('/cars', methods=['GET'])
def get_car_list():
    # Crée un curseur pour exécuter des requêtes SQL
    cursor = mysql.connection.cursor()
    # Sélectionne tous les voitures de la table book
    cursor.execute('SELECT * FROM car')
    # Récupère tous les résultats de la requête
    cars = cursor.fetchall()
    # Convertit les résultats en JSON et les renvoie
    return jsonify(cars)

# Bonne pratique - requête préparée
@app.route('/cars/<int:id>', methods=['GET'])
def get_car(id):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM car WHERE car_id = %s', (id,))
    car = cursor.fetchone()
    if car:
        return jsonify(car)
    else:
        return 'voiture non trouvée', 404
    
# création d'une voiture
@app.route('/cars', methods=['POST'])
def add_car():
    # Récupère les données envoyées en JSON
    data = request.get_json()
    # Extrait le titre du JSON
    brand = data['brand']
    # Extrait l'auteur du JSON
    model = data['model']
    cursor = mysql.connection.cursor()
    # Insère les données dans la base de données
    cursor.execute('INSERT INTO car (brand, model) VALUES (%s, %s)', (brand, model))
    # Applique les modifications
    mysql.connection.commit()
    return 'voiture ajouté', 201

# mise à jour d'une voiture
@app.route('/cars/<int:id>', methods=['PUT'])
def update_car(id):
    data = request.get_json()
    brand = data['brand']
    model = data['model']
    cursor = mysql.connection.cursor()
    # Met à jour les informations du voiture
    cursor.execute('UPDATE car SET brand = %s, model = %s WHERE car_id = %s', (brand, model, id))
    mysql.connection.commit()
    return 'voiture mis à jour', 200

# suppression d'une voiture
@app.route('/cars/<int:id>', methods=['DELETE'])
def delete_car(id):
    cursor = mysql.connection.cursor()
    # Supprime la voiture de la base de données
    cursor.execute('DELETE FROM car WHERE car_id = %s', (id,))
    mysql.connection.commit()
    return 'voiture supprimée', 200

#requête qui permet de trouver les voitures d'une marque (brand) par son nom.
@app.route('/cars/brand/<string:brand>', methods=['GET'])
def get_cars_brand(brand):
    cursor = mysql.connection.cursor()
    # Utilise une requête préparée, plus sécurisée
    cursor.execute('SELECT * FROM car WHERE brand = %s', (brand,))
    car = cursor.fetchall()
    if car:
        return jsonify(car)
    else:
        return 'Marque non trouvée', 404
    
